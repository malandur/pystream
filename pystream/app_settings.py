from __future__ import absolute_import

import argparse
import io
import yaml


def load_properties():
    cfg = load_cfg("resources/application.yaml")
    parser = argparse.ArgumentParser()
    parser.add_argument('--config-file', help='config file override')
    args = parser.parse_args()
    if "config-file" in args:
        temp_cfg = load_cfg(args["config-file"])
        cfg.update(temp_cfg)
    return cfg


def load_cfg(file_path):
    with io.open(file_path) as f:
        cfg = yaml.load(f)
        return cfg
