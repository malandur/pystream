
class ObjectKeyExists(Exception):

    def __init__(self, message):
        super(ObjectKeyExists, self).__init__(message)