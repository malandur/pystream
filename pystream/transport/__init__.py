from __future__ import absolute_import

import zmq
from logbook import Logger
from zmq.utils.monitor import parse_monitor_message

from pystream.containers import ConfigManager, AppConfiguration

logger = Logger(__name__)

from enum import Enum
from stream_proto import Read, ReadAck
import asyncio


class SocketStatus(Enum):
    CREATED = 1
    CONNECTED = 2
    SYNCED = 3
    DISCONNECTED = 4


class Transport(object):

    def __init__(self, ctx, endpoint, receiver=False, receive_callback=None, record_manager=None):
        self.endpoint = endpoint
        self.ctx = ctx
        self.receiver = receiver
        self.receive_callback = receive_callback
        self.__lock=asyncio.Lock()
        self.__socket = None
        self.__monitor_socket = None
        self.__state = None
        self.__status = SocketStatus.CREATED
        self.__serializer = AppConfiguration.serializer()
        self.__record_manager = record_manager
        self.__read_idx = -1

    @property
    def status(self):
        return self.__status

    @property
    def socket(self):
        return self.__socket

    @property
    def description(self):
        return self.__state

    async def initialize(self):
        socket_type = zmq.PAIR
        self.__socket = self.ctx.socket(socket_type)
        self.__socket.connect(self.endpoint) if self.receiver else self.__socket.bind(self.endpoint)
        self.__monitor_socket = self.__socket.get_monitor_socket()

    async def manage_monitor(self):
        evt_map = ConfigManager.zmq_error_map()
        while True:
            while True:
                data = await self.__monitor_socket.recv_multipart(flags=0)
                evt = parse_monitor_message(data)
                self.__state = evt_map.event_map[evt['event']]
                self.__update_state()
                evt.update({'description': self.__state, "receiver": self.receiver})
                logger.info("Event: {}".format(evt))
                if evt['event'] == zmq.EVENT_MONITOR_STOPPED:
                    break
            self.__monitor_socket.close()
            logger.info("event monitor thread done!")
            logger.info("getting a new monitor socket")
            self.__monitor_socket = self.__socket.get_monitor_socket()

    def __update_state(self):
        self.__read_idx = -1
        if self.__state == 'EVENT_ACCEPTED' or self.__state == 'EVENT_CONNECTED':
            self.__status = SocketStatus.CONNECTED
        else:
            self.__status = SocketStatus.DISCONNECTED

    async def receive(self):
        while True:
            payload = await self.__socket.recv()
            # logger.debug("Received payload")
            _,payload = self.__serializer.deserialize(payload)
            await self.__handle_socket(payload)

    async def __handle_socket(self, payload):
        if isinstance(payload, Read):
            if self.__read_idx != payload.read_index or payload.read_index == 0:
                with await self.__lock:
                    self.__read_idx = payload.read_index
                    async for data in self.__record_manager.read(payload.read_index):
                        await self.send_bytes(data)
        elif isinstance(payload, ReadAck):
            self.__record_manager.read_ack(payload.read_index)
        else:
            await self.receive_callback(payload)
            # Increment after processing
            self.__record_manager.inc_read_index()

    async def timer_task(self):
        while True:
            await asyncio.sleep(0.5)
            read_idx = self.__record_manager.read_index()
            if read_idx == 0:
                read = Read()
                read.read_index = 0
                await self.send(read)
            else:
                read_ack = ReadAck()
                read_ack.read_index = self.__record_manager.read_index()
                read = Read()
                read.read_index = self.__record_manager.next_read_index()
                await self.send(read_ack)
                await self.send(read)

    async def send_payload(self, payload):
        with await self.__lock:
            write_index=self.__record_manager.write(payload)
            payload = self.__serializer.serialize(payload,write_index)
            await self.send_bytes(payload)

    async def send(self, payload):
        payload = self.__serializer.serialize(payload)
        await self.send_bytes(payload)

    async def send_bytes(self, payload):
        await self.__socket.send(payload)
        # if self.status == SocketStatus.CONNECTED:
        # logger.debug("Sending payload")
        #     await self.__socket.send(payload)
        # else:
        #     logger.debug("Not connected")
