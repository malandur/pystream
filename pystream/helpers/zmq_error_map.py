from __future__ import absolute_import

import zmq


class ZmqErrorMap(object):

    def __init__(self):
        self._event_map = {}
        self.initialize()

    def initialize(self):
        for name in dir(zmq):
            if name.startswith('EVENT_'):
                value = getattr(zmq, name)
                self._event_map[value] = name

    @property
    def event_map(self):
        return self._event_map
