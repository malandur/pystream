import struct

import bitmath


def get_struct(type):
    if type is "small":
        return struct.Struct("H")
    # if size < 63 kb
    elif type is "medium":
        return struct.Struct("I")
    elif type is "large":
        return struct.Struct("I")
    else:
        return struct.Struct("Q")


def get_bytes_size(size, type):
    if type == "KB":
        return bitmath.KiB(size).bytes
    elif type == "MB":
        return bitmath.MiB(size).bytes
    elif type == "GB":
        return bitmath.GiB(size).bytes
    return size
