from __future__ import absolute_import

import asyncio

from logbook import Logger
from stream_proto import HeartBeat, Read, ReadAck

from pystream.app_settings import load_properties
from pystream.config.logging import config_logging
from pystream.containers import ConfigManager
from pystream.test_sample.sample_reciever import SampleReciever
from pystream.test_sample.sample_sender import SampleSender

# from stream_proto import HeartBeat,Sync,Connect,Read,ReadAck

if __name__ == "__main__":
    ConfigManager.config.override({"class_list": [HeartBeat, Read, ReadAck], "directory": "/home/mithu/boxes/receiver",
                                   "index_manager": {"name": "index_manager", "max_records": 100000}})

    print(ConfigManager.zmq_error_map().event_map)
    print(ConfigManager.config.directory())
    properties = load_properties()

    config_logging("dev")
    logger = Logger(__name__)


    # s = SampleSender()
    # s.run_connection_init()

    s2 = SampleReciever()
    s2.run_connection_init()


    # asyncio.ensure_future(s1.start())

    ConfigManager.event_loop().run_forever()

    # logger.info(ConfigManager.test_key())
    # run()
