from __future__ import absolute_import

from pystream.containers import ConfigManager


class Base:

    def register(self, key, object):
        ConfigManager.serializer.register(key, object)

    async def initialize(self):
        pass

    def run_connection_init(self):
        pass
