from __future__ import absolute_import

import asyncio

from pystream.containers import ConfigManager, AppConfiguration
from pystream.interfaces import Base
from pystream.storage.record_manager import record_manager_builder
from pystream.transport import Transport


class Receiver(Base):

    def __init__(self, stream_name, index, endpoint):
        self.receiver_stream_name = stream_name
        ctx = ConfigManager.zmq_context()
        self.receiver_endpoint = Transport(ctx, endpoint, receiver=True, receive_callback=self.__receive,
                                           record_manager=record_manager_builder(stream_name, index, False))

    async def __receive(self, payload):
        await self.receive(payload)

    async def receive(self, data):
        raise NotImplemented("Error")

    def run_connection_init(self):
        ConfigManager.event_loop().run_until_complete(asyncio.wait([self.receiver_endpoint.initialize()]))
        # Start the receiver
        asyncio.ensure_future(self.receiver_endpoint.manage_monitor())
        asyncio.ensure_future(self.receiver_endpoint.receive())
        asyncio.ensure_future(self.receiver_endpoint.timer_task())
        ConfigManager.event_loop().run_until_complete(asyncio.wait([self.initialize()]))
