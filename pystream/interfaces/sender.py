from __future__ import absolute_import

import asyncio

from pystream.containers import ConfigManager
from pystream.interfaces import Base
from pystream.interfaces.sender_type import sender_factory
from pystream.storage.record_manager import record_manager_builder
from pystream.transport import Transport


class Sender(Base):

    def __init__(self, stream_name, endpoints, stream_type):
        self.sender_stream_name = stream_name
        ctx = ConfigManager.zmq_context()
        self.sender_endpoints = []
        for idx, endpoint in enumerate(endpoints):
            transport = Transport(ctx, endpoint,
                                  record_manager=record_manager_builder(stream_name, idx + 1, True))
            self.sender_endpoints.append(transport)

        self.sender = sender_factory(stream_type, self.sender_endpoints)

    async def send(self, data, key=None):
        # payload = self.sender_serialize(data)
        await self.sender.send(data, key)

    async def start(self):
        pass

    def run_connection_init(self):
        ConfigManager.event_loop().run_until_complete(
            asyncio.wait([endpoint.initialize() for endpoint in self.sender_endpoints]))
        # Create the monitor socket
        [asyncio.ensure_future(endpoint.manage_monitor()) for endpoint in self.sender_endpoints]
        # Start the receivers
        [asyncio.ensure_future(endpoint.receive()) for endpoint in self.sender_endpoints]

        ConfigManager.event_loop().run_until_complete(asyncio.wait([self.initialize()]))
        asyncio.ensure_future(self.start())
