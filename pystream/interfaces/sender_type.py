from __future__ import absolute_import

import mmh3


class SenderType(object):

    def __init__(self, endpoints):
        self.endpoints = endpoints

    async def send(self, payload, key):
        raise NotImplemented("Implement send")


class KeyHashSender(SenderType):

    def __init__(self, endpoints):
        super(KeyHashSender, self).__init__(endpoints)

    async def send(self, payload, key):
        key_id = mmh3.hash(key, signed=False) % len(self.endpoints)
        await self.endpoints[key_id].send_payload(payload)


class BroadcastSender(SenderType):

    def __init__(self, endpoints):
        super(BroadcastSender, self).__init__(endpoints)

    async def send(self, payload, key):
        for endpoint in self.endpoints:
            await endpoint.send_payload(payload)


class RoundRobinSender(SenderType):

    def __init__(self, endpoints):
        super(RoundRobinSender, self).__init__(endpoints)
        self.counter = 0
        self.endpoints_count = len(endpoints)

    async def send(self, payload, key):
        await self.endpoints[self.counter].send_payload(payload)
        self.counter = ((self.counter + 1) % self.endpoints_count)


def sender_factory(key, endpoints):
    if key == "keyhash":
        return KeyHashSender(endpoints)
    elif key == "broadcast":
        return BroadcastSender(endpoints)
    return RoundRobinSender(endpoints)
