from __future__ import absolute_import

import asyncio

from pystream.containers import ConfigManager
from pystream.interfaces.receiver import Receiver
from pystream.interfaces.sender import Sender


class TransReciever(Receiver, Sender):

    def __init__(self, receiver_stream_name, receiver_endpoint, sender_stream_name, sender_endpoints,
                 sender_stream_type):
        Receiver.__init__(self, receiver_stream_name, receiver_endpoint)
        Sender.__init__(self, sender_stream_name, sender_endpoints, sender_stream_type)

    def run_connection_init(self):
        ConfigManager.event_loop().run_until_complete(
            asyncio.wait([endpoint.initialize() for endpoint in self.sender_endpoints]))
        [asyncio.ensure_future(endpoint.manage_monitor()) for endpoint in self.sender_endpoints]
        ConfigManager.event_loop().run_until_complete(asyncio.wait([self.receiver_endpoint.initialize()]))
        # Start the receiver
        asyncio.ensure_future(self.receiver_endpoint.receive())
        asyncio.ensure_future(self.receiver_endpoint.timer_task())
        ConfigManager.event_loop().run_until_complete(asyncio.wait([self.initialize()]))
