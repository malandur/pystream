from __future__ import absolute_import

import sys

from logbook import RotatingFileHandler, StreamHandler
from logbook.base import DEBUG, INFO


def config_logging(env="dev"):
    if env == "dev":
        StreamHandler(sys.stdout, level=DEBUG).push_application()
    else:
        RotatingFileHandler("/home/mithu/stream.log", level=INFO, backup_count=10, bubble=True).push_application()
    # setup.push_application()
