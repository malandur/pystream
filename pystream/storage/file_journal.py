from __future__ import absolute_import

from pathlib import Path

import pyarrow


class FileJournal(object):

    def __init__(self, path, size, write_index=0):
        my_file = Path(path)
        if not my_file.is_file():
            pyarrow.MemoryMappedFile.create(path, size)
        self.reader = pyarrow.memory_map(path, 'rb')
        self.writer = pyarrow.memory_map(path, 'rb+')
        self.writer.seek(write_index)

    def write(self, payload):
        start_index = self.writer.tell()
        self.writer.write(payload)
        return start_index, self.writer.tell()

    def read(self, start_index, end_index):
        self.reader.seek(start_index)
        return self.reader.read(end_index - start_index)

    def flush(self):
        self.writer.flush()

    def reset_writer(self):
        self.writer.seek(0)
