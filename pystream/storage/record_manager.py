from __future__ import absolute_import

import os

from pystream.containers import AppConfiguration, ConfigManager
from pystream.storage.file_journal import FileJournal
from pystream.storage.record_store import RecordStore


class RecordManager(object):

    def __init__(self, name, index, record_store=None, file_journal=None):
        self.record_store = record_store
        self.name, self.index = name, index
        self.file_journal = file_journal
        self.index_manager = AppConfiguration.index_manager()

    def write(self, payload):
        index = self.index_manager.write_index(self.name, self.index)
        (start_idx, end_idx) = self.file_journal.write(payload)
        self.record_store[index] = (start_idx, end_idx)
        return self.index_manager.inc_write_index(self.name, self.index)

    async def read(self, index):
        idx = self.index_manager.loop_write_index(self.name, self.index)
        if ConfigManager.config.index_manager.max_records() == idx + 1:
            return
        while index < idx:
            start_idx, end_idx = self.record_store[index]
            index = (index + 1) % ConfigManager.config.index_manager.max_records()
            if start_idx is not None and end_idx is not None:
                yield self.file_journal.read(start_idx, end_idx)

    def read_ack(self, idx):
        self.index_manager.inc_read_index(self.name, self.index, idx)

    def read_index(self):
        return self.index_manager.read_index(self.name, self.index)

    def inc_read_index(self):
        self.index_manager.inc_read_index(self.name, self.index, 1)

    def next_read_index(self):
        return self.index_manager.next_read_index(self.name, self.index)


def record_manager_builder(name, index, sender=True):
    directory = ConfigManager.config.directory()
    if sender:
        record_store_path = os.path.join(directory, "{}_{}_record".format(name, index))

        record_store = RecordStore(record_store_path)
        journal_store_path = os.path.join(directory, "{}_{}_journal".format(name, index))

        file_journal = FileJournal(journal_store_path, 102400,
                                   AppConfiguration.index_manager().write_index(name, index))
        return RecordManager(name, index, record_store, file_journal)
    else:
        return RecordManager(name, index)
