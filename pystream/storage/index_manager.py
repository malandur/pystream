from __future__ import absolute_import

import os
import struct

import plyvel


class IndexManager(object):

    def __init__(self, directory, name, max_records=100):
        file_path = os.path.join(directory, name)
        self.record_store = IndexStore(file_path)
        self.max_records = max_records
        self.read_template = "read_{}_{}"
        self.write_template = "write_{}_{}"

    def read_index(self, name, index):
        key = self.read_template.format(name, index)
        return self.record_store[key]

    def write_index(self, name, index):
        key = self.write_template.format(name, index)
        return self.record_store[key]

    def inc_read_index(self, name, index, n):
        key = self.read_template.format(name, index)
        v = (self.record_store[key] + n) % self.max_records
        self.record_store[key] = v

    def next_read_index(self, name, index):
        key = self.read_template.format(name, index)
        return (self.record_store[key] + 1) % self.max_records

    def inc_write_index(self, name, index):
        key = self.write_template.format(name, index)
        v = (self.record_store[key] + 1) % self.max_records
        self.record_store[key] = v
        return v

    def loop_write_index(self, name, index):
        key = self.write_template.format(name, index)
        v = (self.record_store[key] - 1) % self.max_records
        return v


class IndexStore(object):

    def __init__(self, directory):
        self.__db = plyvel.DB(directory, create_if_missing=True)
        self.index_struct = struct.Struct(">Q")

    @property
    def db(self):
        return self.__db

    def __getitem__(self, key):
        value = self.__db.get(key.encode(), self.index_struct.pack(0))
        (v,) = self.index_struct.unpack(value)
        return v

    def __setitem__(self, key, value):
        self.__db.put(key.encode(), self.index_struct.pack(value))

    def __delitem__(self, key):
        self.__db.delete(key.encode())

    def __contains__(self, key):
        return self[key] is not None

    def __del__(self):
        self.__db.close()
