from __future__ import absolute_import

import struct

import plyvel


class RecordStore(object):

    def __init__(self, file_path):
        self.__db = plyvel.DB(file_path, create_if_missing=True)
        self.template = "%010d"
        self.record_struct = struct.Struct(">QQ")

    @property
    def db(self):
        return self.__db

    def __getitem__(self, key):
        key = (self.template % key).encode()
        value = self.__db.get(key)
        if value is not None:
            return self.record_struct.unpack(value)
        return None, None

    def __setitem__(self, key, value):
        key = (self.template % key).encode()
        self.__db.put(key, self.record_struct.pack(*value))

    def __delitem__(self, key):
        key = (self.template % key).encode()
        self.__db.delete(key)

    def __contains__(self, key):
        return self[key] is not None

    def __del__(self):
        self.__db.close()
