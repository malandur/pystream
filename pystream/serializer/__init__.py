from __future__ import absolute_import

import struct

from bidict import bidict
from logbook import Logger

from pystream.exception.object_key_exists import ObjectKeyExists

logger = Logger(__name__)


class Serializer:

    def __init__(self, class_list):
        self.override = False
        self.msgHead = struct.Struct(">BQ")
        self.serializer = bidict()
        self.keys = set()
        for idx, val in enumerate(class_list):
            self.__register(idx + 1, val)

    def __register(self, key, object):
        if key not in self.serializer.keys():
            self.serializer[key] = object
        else:
            raise ObjectKeyExists("Key already exists")

    def register(self, key, object):
        """
        Register object with int key
        :param key (int): registry number
        :param object:  object to be sent
        :return:
        """
        self.__register(key, object)

    def serialize(self, obj, read_index=0):
        t = type(obj)
        key = self.serializer.inv[t]
        return self.msgHead.pack(key, read_index) + obj.SerializeToString()

    def deserialize(self, data):
        (i, read_index) = self.msgHead.unpack(data[:self.msgHead.size])
        d = data[self.msgHead.size:]
        obj = self.serializer[i]
        o1 = obj()
        o1.ParseFromString(d)
        return read_index, o1
