from __future__ import absolute_import

import threading

from aiohttp import web
from flask import Flask

app = Flask(__name__)



class MonitorEndpoint(threading.Thread):

    def __init__(self, port, host="localhost"):
        super(MonitorEndpoint, self).__init__()
        self.port = port
        self.host = host
        self.daemon = True
        self.name = "Http-Monitor-Thread"

    @app.route("/")
    async def index(self, _):
        return web.Response(text='Hello Aiohttp!')


    def run(self):
        app.run()
#
# import time
#
# t = MonitorEndpoint(3000)
# t.start()
# time.sleep(12222)
