from __future__ import absolute_import

import asyncio

import dependency_injector.containers as containers
import dependency_injector.providers as providers
import zmq.asyncio

from pystream.helpers.zmq_error_map import ZmqErrorMap
from pystream.storage.index_manager import IndexManager
from pystream.serializer import Serializer


class ConfigManager(containers.DeclarativeContainer):
    """IoC container of movies module component providers."""

    zmq_error_map = providers.Singleton(ZmqErrorMap)
    zmq_context = providers.Object(zmq.asyncio.Context())
    event_loop = providers.Object(asyncio.get_event_loop())
    config = providers.Configuration('config')


class AppConfiguration(containers.DeclarativeContainer):
    serializer = providers.Singleton(Serializer, class_list=ConfigManager.config.class_list)
    index_manager = providers.Singleton(IndexManager, directory=ConfigManager.config.directory,
                                        name=ConfigManager.config.index_manager.name,
                                        max_records=ConfigManager.config.index_manager.max_records)
