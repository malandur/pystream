from pystream.interfaces.transreciever import TransReciever


class SampleTransreciever(TransReciever):

    def __init__(self):
        super(SampleTransreciever, self).__init__("sampler", "tcp://127.0.0.1:30000", "sampler2",
                                                ["tcp://127.0.0.1:40000"], "keyhash")

    async def receive(self, data):
        await self.send(data, "")
