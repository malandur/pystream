import zmq
from transport import Transport
import asyncio

ctx = zmq.asyncio.Context()
t1=Transport(ctx,"tcp://*:20000",receiver=False)
asyncio.get_event_loop().run_until_complete(asyncio.wait([
    t1.initialize()
]))

asyncio.get_event_loop().run_until_complete(asyncio.wait([
    t1.wait_send()
]))
