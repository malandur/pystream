import asyncio

from stream_proto import HeartBeat

from pystream.interfaces.sender import Sender


class SampleSender(Sender):

    def __init__(self):
        super(SampleSender, self).__init__("sampler", ["tcp://127.0.0.1:30000"], "keyhash")

    async def start(self):
        while True:
            await asyncio.sleep(1)
            h = HeartBeat()
            h.acked = 12
            await self.send(h, "")
