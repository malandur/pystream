from __future__ import absolute_import

import asyncio

import zmq.asyncio

from pystream.transport import Transport

def mycallback(data):
    print(data)


def run():
    ctx = zmq.asyncio.Context()
    t3 = Transport(ctx, "tcp://*:12000", receiver=False)
    ctx = zmq.asyncio.Context()
    t1 = Transport(ctx, "tcp://localhost:12000", receiver=True, receive_callback=mycallback)
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    asyncio.get_event_loop().run_until_complete(asyncio.wait([
        t3.initialize(),
        t1.initialize()
    ]))

    asyncio.get_event_loop().run_until_complete(asyncio.wait([
        t3.manage_monitor(),
        t3.wait_send(),
        t1.receive()
    ]))
