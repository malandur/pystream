#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `pystream` package."""


import unittest

from pystream import app_settings


class TestPystream(unittest.TestCase):
    """Tests for `pystream` package."""

    def setUp(self):
        """Set up test_sample fixtures, if any."""

    def tearDown(self):
        """Tear down test_sample fixtures, if any."""

    def test_000_something(self):
        """Test something."""
